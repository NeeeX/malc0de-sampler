# Malc0de Sampler #

The script fetches the malc0de.com page containing the list of URL's with malicious PE binaries (samples) and lookups the binary hash for VT detection ratio.
If the PE is not commonly detected, it is submitted to the internal sandbox system for analysis. 

SQLite database is used to separate already analysed and new samples.