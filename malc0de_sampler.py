#xx.xx.2010

import urllib
import urllib2
import re
import sqlite3
import argparse
from time import time

def get_vt_detection(mal_hash):
	url = "https://www.virustotal.com/file/%s/analysis/" % (mal_hash)
	data = urllib2.urlopen(url).read()
	if data:
		dt_obj = re.compile(r"<td class=\" text-red \">([0-9]{1,2}) / [0-9]{2}<\/td>", re.MULTILINE)
		dt_mo  = dt_obj.findall(data)
		if len(dt_mo) == 1:
			return dt_mo[0]

		
	return -1

def delete_entries(conn):
	conn.execute("DELETE FROM processed")
	conn.commit()
	return True

def insert_entry(conn, mal_hash, mal_url, mal_ratio, mal_type):
	conn.execute("INSERT INTO processed (date, hash, url, ratio, type) VALUES(%d, '%s', '%s', %d, %d)" % (int(time()), mal_hash, mal_url, mal_ratio, mal_type))
	conn.commit()
	return True


def entry_exists(conn, mal_hash):
	row = conn.execute("SELECT COUNT(*) FROM processed WHERE hash = '%s'" % (mal_hash))
	if (row.fetchone()[0] > 0):
		return True
	else:
		return False

cfg_up_url     = "http://5.135.152.172/upload.php"
cfg_maximum_vt = 15

optParser = argparse.ArgumentParser(description='Fetches urls from malc0de and submits them for analysis')
optParser.add_argument('-reset',		action="store_true", dest="reset", default= 0, help = "Truncates DB")
optParser.add_argument('-page',			action="store", type=int, dest="pagenum", default=1, help =  'Number of a page to fetch')
optParser.add_argument('-limit',		action="store", type=int, dest="limit", help =  'Limit number of processed urls')

arguments = optParser.parse_args()

db = sqlite3.connect('samples.db')

if not db:
	print "Unable to open database"
	exit()

if arguments.reset:
	delete_entries(db)
	print "Successfully trunaceted the database!"
	exit()

data = urllib2.urlopen("http://malc0de.com/database/?&page=%d" % (arguments.pagenum)).read()

if data:
	obj = re.compile(r"<tr class=\"class1\"\>\s<td>(.*?)</td>\s<td>(.*?)</td>", re.MULTILINE)
	url_mo = obj.findall(data)

	vt_regex = re.compile(r"\<a href='https:\/\/www.virustotal.com\/file\/([abcdef0-9]*?)\/analysis\/\'\>", re.MULTILINE)
	vt_mo = vt_regex.findall(data)

	if arguments.limit:
		num_elements = arguments.limit
	else:
		num_elements = len(url_mo)

	print "Num elements - %d" % (num_elements)

	for i in xrange(0, num_elements):
		entry_url	= url_mo[i][1]
		entry_date	= url_mo[i][0]
		entry_md5	= vt_mo[i]
		entry_url   = entry_url.replace('<br/>', '')

		
		if entry_exists(db, entry_md5):
			print "[%d] Entry %s already exists" % (i, entry_md5)
			continue

		entry_ratio	= int(get_vt_detection(entry_md5))

		if (entry_ratio > cfg_maximum_vt) and (entry_ratio != -1):
			print "[%d] Entry %s ratio %d is above defined limit" % (i, entry_md5, entry_ratio)
			insert_entry(db, entry_md5, entry_url, int(entry_ratio),  2)
			continue

		post_data = {'act'	   : 'fileDownload',
					 'url'	   : entry_url,
					 'free'	   : '0',
					 'timeout' : ''}

		data = urllib.urlencode(post_data)
		req  = urllib2.Request(cfg_up_url, data)
		resp = urllib2.urlopen(req)

		if not resp:
			continue
		else:
			resp = resp.read()

		if resp.find('analysis!') > 0:
			print "[%d] Entry %s submitted to analysis!" % (i, entry_md5)
			insert_entry(db, entry_md5, entry_url, int(entry_ratio),  5)
		else:
			print "[%d] Entry %s failed being submitted to analysis!" % (i, entry_md5)